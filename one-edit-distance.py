class Solution:
    # @param s, a string
    # @param t, a string
    # @return a boolean
    def isOneEditDistance(self, s, t):
        sLen = len(s); tLen = len(t)
        if sLen > tLen:
            return self.isOneEditDistance(t, s)
        if tLen - sLen > 1:
            return False
        i = 0; shift = tLen - sLen
        while i < sLen and s[i] == t[i]:
            i += 1
        if i == sLen:
            return shift > 0
        if shift == 0:
            i += 1
        while i < sLen and s[i] == t[i + shift]:
            i += 1
        return i == sLen