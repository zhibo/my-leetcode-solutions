# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class LevelNode:
    def __init__(self, node, level):
        self.node = node
        self.level = level

class Solution:
    # @param root, a tree node
    # @return a list of lists of integers
    def levelOrder(self, root):
        results = []
        nodes = []
        levelnodes = []
        
        if not root:
            return results
        
        currentlevel = 0

        node = root
        level = 0
        levelnode = LevelNode(node, level)
        
        levelnodes.append(levelnode)

        while levelnodes:
            levelnode = levelnodes[0]
            node = levelnode.node
            level = levelnode.level

            if currentlevel == level:
                nodes.append(node.val)
                levelnodes.pop(0)
            else:
                results.append(nodes)
                nodes = []
                currentlevel += 1
                continue
            
            if node.left:
                levelnodes.append(LevelNode(node.left, level+1))
            if node.right:
                levelnodes.append(LevelNode(node.right, level+1))

            if not levelnodes and nodes:
                results.append(nodes)
        
        return results