# @param {Integer} m
# @param {Integer} n
# @return {Integer}
def unique_paths(m, n)
    f = Array.new(m, Array.new(n, 0))
    for i in 0..m-1 do
        f[i][0] = 1
    end
    for i in 0..n-1 do
        f[0][i] = 1
    end
    
    for i in 1..m-1 do
        for j in 1..n-1 do 
            f[i][j] = f[i - 1][j] + f[i][j - 1]
        end
    end
    return f[m - 1][n - 1]
end
