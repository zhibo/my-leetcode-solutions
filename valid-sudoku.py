class Solution:
    # @param board, a 9x9 2D array
    # @return a boolean
    def isValidSudoku(self, board):
        rList = []
        cList = []
        gList = []
        vldMap = {}
        for i in range(1, 10):
            vldMap[chr(i + ord('0'))] = 0
        for i in range(0, 9):
            rList.append(vldMap.copy())
            cList.append(vldMap.copy())
            gList.append(vldMap.copy())
        
        for i in range(0, 9):
            for j in range(0, 9):
                target = board[i][j]
                if target == '.':
                    continue
                
                if rList[i][target] == 0:
                    rList[i][target] = 1
                else:
                    return False
                
                if cList[j][target] == 0:
                    cList[j][target] = 1
                else:
                    return False
                
                gTarget = 3 * (i / 3) + (j / 3)
                if gList[gTarget][target] == 0:
                    gList[gTarget][target] = 1
                else:
                    return False
        
        return True