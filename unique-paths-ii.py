class Solution:
    # @param obstacleGrid, a list of lists of integers
    # @return an integer
    def uniquePathsWithObstacles(self, obstacleGrid):
        m = len(obstacleGrid); n = len(obstacleGrid[0])
        # don't use shallow copies
        f = [[0 for i in range(n)] for j in range(m)]
        f[0][0] = 0 if obstacleGrid[0][0] else 1
        for i in range(1, m):
            f[i][0] = 0 if obstacleGrid[i][0] else f[i - 1][0]
        for i in range(1, n):
            f[0][i] = 0 if obstacleGrid[0][i] else f[0][i - 1]
        
        for i in range(1, m):
            for j in range(1, n):
                f[i][j] = 0 if obstacleGrid[i][j] else f[i - 1][j] + f[i][j - 1]
        
        return f[m - 1][n - 1]


s = Solution()
print s.uniquePathsWithObstacles([[0, 0], [1, 0]])
