# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
#         self.next = None

class Solution:
    # @param root, a tree node
    # @return nothing
    def connect(self, root):
        if not root:
            return
        
        nxt = root
        
        while nxt:
            cur = nxt
            nxt = tmpNode = None
            while cur:
                if cur.left:
                    if not tmpNode:
                        tmpNode = cur.left
                        nxt = tmpNode
                    else:
                        tmpNode.next = cur.left
                        tmpNode = tmpNode.next
                if cur.right:
                    if not tmpNode:
                        tmpNode = cur.right
                        nxt = tmpNode
                    else:
                        tmpNode.next = cur.right
                        tmpNode = tmpNode.next
                    
                cur = cur.next