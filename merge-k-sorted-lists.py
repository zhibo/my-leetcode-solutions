# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @param {ListNode[]} lists
    # @return {ListNode}
    def mergeKLists(self, lists):
        p = h = ListNode(0)
        q = [(list.val, list) for list in lists if list]
        heapq.heapify(q)
        while q:
            p.next = heapq.heappop(q)[1]
            p = p.next
            if p.next:
                heapq.heappush(q, (p.next.val, p.next))
        return h.next
