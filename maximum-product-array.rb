# @param {Integer[]} nums
# @return {Integer}
def max_product(nums)
    hereMax, hereMin, resMax = nums[0], nums[0], nums[0]
    nums.slice(1, nums.length()).each{|x| 
        tMax = hereMax; tMin = hereMin;
        hereMax = [x, tMax * x, tMin * x].max;
        hereMin = [x, tMin * x, tMax * x].min;
        resMax = [hereMax, resMax].max
    }
    return resMax
end
