# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
#
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @param head, a list node
    # @return a tree node
    l = None
    def buildBST(self, left, right):
        if left > right:
            return None
        mid = (left + right) / 2
        leftNode = self.buildBST(left, mid - 1)
        parentNode = TreeNode(self.l.val)
        parentNode.left = leftNode
        self.l = self.l.next
        rightNode = self.buildBST(mid + 1, right)
        parentNode.right = rightNode
        return parentNode
        
    def sortedListToBST(self, head):
        length = 0; p = head
        while p:
            length += 1
            p = p.next
        self.l = head
            
        return self.buildBST(0, length - 1)
