### NOTE: not Accepted(Runtime Error) ###
# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

import sys

class Solution:
    # @param root, a tree node
    # @return an integer
    maxSum = 0
    
    def findMax(self, node):
        if not node:
            return 0
        left = self.maxPathSum(node.left)
        right = self.maxPathSum(node.right)
        self.maxSum = max(node.val,\
                    (node.val + left),\
                    (node.val + right),\
                    (node.val + left + right),\
                    self.maxSum)
        retval = max(node.val, node.val + left, node.val + right)
        return retval
        
    def maxPathSum(self, root):
        self.maxSum = -1 * sys.maxint
        self.findMax(root)
        return self.maxSum
