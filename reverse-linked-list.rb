class ListNode
  attr_accessor :val, :next
  def initialize(val)
    @val = val
    @next = nil
  end
end

def reverse_linked_list(head)
    k, p, q = nil, head, head.next
    while p != nil do
        p.next = k
        k = p
        p = q
        q = q.next if q
    end
    return k
end

head = ListNode.new(0)

p = head
while p do
  puts p.val
  p = p.next
end

res = reverse_linked_list(head)

p = res
while p do
  puts p.val
  p = p.next
end
