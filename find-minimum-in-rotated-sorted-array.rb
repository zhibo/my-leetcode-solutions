# @param {Integer[]} nums
# @return {Integer}
def find_min(nums)
    l, r = 0, nums.length() - 1
    return nums[l] if nums[l] < nums[r]

    nl, nr = nums[0], nums[r]
    while l < r do
        return [nums[l], nums[r]].min if r - l == 1

        m = (l + r) / 2
        return nums[m] if nums[m - 1] > nums[m] and nums[m] < nums[m + 1]
        begin l = m; next end if nums[m] > nr
        begin r = m; next end if nums[m] < nl        
    end

    return nums[0]
end

# Simpler version
def find_min2(nums)
    l, r = 0, nums.length() - 1
    while l < r and nums[l] >= nums[r]
        m = (l + r) / 2
        if nums[m] > nums[r]
            l = m + 1
        else
            r = m
        end
    end
    return nums[l]
end

puts find_min([2, 1])
