// Forward declaration of the read4 API.
int read4(char *buf);

class Solution {
public:
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    int read(char *buf, int n) {
        int r = 0, s = 0;
        while (n - s >= 4){
            r = read4(buf + s);
            s += r;
            if (r < 4){
                return s; 
            }
        }
        char buf2[4] = {0};
        r = read4(buf2);
        int last = r > n - s ? n - s : r;
        strncpy(buf + s, buf2, last);
        return s + last;
    }
};