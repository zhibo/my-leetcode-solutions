# Definition for singly-linked list with a random pointer.


class RandomListNode:
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None


class Solution:
    
    # @param head, a RandomListNode
    # @return a RandomListNode
    def copyRandomList(self, head):
        p = d = RandomListNode(0)
        d.next = head
        p = d.next
        
        if not head:
            return d.next
            
        while p:
            n = RandomListNode(p.label)
            n.next = p.next
            p.next = n
            p = n.next
            
        p = d.next
        while p:
            n = p.next
            if p.random:
                n.random = p.random.next
            p = n.next
        
        p = d.next
        d.next = p.next
        while p:
            n = p.next
            p.next = n.next
            p = p.next
            if p:
                n.next = p.next
              
        return d.next


t = RandomListNode(0)

s = Solution()

print s.copyRandomList(t)
