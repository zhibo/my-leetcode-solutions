# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param root, a tree node
    # @return a boolean
    def maxDepth(self, root):
        if not root:
            return 0
        l = self.maxDepth(root.left)
        if l == -1:
            return -1
        r = self.maxDepth(root.right)
        if r == -1:
            return -1
        return abs(l - r) <= 1 and max(l, r) + 1 or -1
        
    def isBalanced(self, root):
        return self.maxDepth(root) != -1
