class Solution:
    # @param A, a list of integers
    # @return an integer
    def firstMissingPositive(self, A):
        aLen = len(A)
        i = 0
        """
        the answer must be in [1, aLen + 1], 
        if the number in not its right position then help swap it to
        """
        while i < aLen:
            if A[i] > 0 and A[i] <= aLen and A[i] != i + 1:
                if A[A[i] - 1] != A[i]:
                    temp = A[A[i] - 1]
                    A[A[i] - 1] = A[i]
                    A[i] = temp
                    i -= 1
                
            i += 1
        
        i = 0
        while i < aLen:
            if A[i] != i + 1:
                break
            i += 1
        
        return i + 1
