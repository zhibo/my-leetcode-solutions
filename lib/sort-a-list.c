#include <stdio.h>
#include <stdlib.h>
#include "list.h"

struct node {
    char ch;
    int val;
    struct list_head list;
};

int main()
{
    struct node head;
    struct node *p = (struct node*)malloc(sizeof(struct node));
    printf("%d\n", (unsigned int)offsetof(struct node, list));
    p->ch = 'a';
    p->val = 3;
    int *intp = &(p->val);
    struct node *q = list_entry(intp, struct node, val);
    // struct node *q = ({ const typeof( ((struct node *)0)->val ) *__mptr = (intp); (struct node *)( (char *)__mptr - ((size_t) (&((struct node *)0)->val)) ); });

    printf("q: %p\n", q);
    printf("q->ch: %c\n", q->ch);
    return 0;
}
