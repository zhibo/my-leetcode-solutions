#ifndef _LIST_H_
#define _LIST_H_

#define offsetof(type, member) ((size_t) (&((type *)0)->member))

#define container_of(ptr, type, member) ({\
        const typeof( ((type *)0)->member ) *__mptr = (ptr); \
        (type *)( (char *)__mptr - offsetof(type, member) ); })

#define list_entry(ptr, type, member)  container_of(ptr, type, member) 

struct list_head {
    struct list_head *next;
    struct list_head *prev;
};

#endif
