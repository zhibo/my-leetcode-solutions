#include <stdio.h>

#define ARR_SIZE 10

void dump_arr(int *a, int l, int r)
{
    int i = l;
    for ( ; i <= r; ++i){
        printf("%d ", a[i]);
    }
    printf("\n");
}

void qsort(int *a, int l, int r)
{
    if ( l >= r ){
        return;
    }

    int i = l, j = r, pivot = a[l];

    while ( i < j ){
        while ( i < j && a[j] >= pivot ){
            --j;
        }
        a[i] = a[j];

        while ( i < j && a[i] <= pivot ){
            ++i;
        }
        a[j] = a[i];
    }
    a[i] = pivot;
    qsort(a, l, i - 1);
    qsort(a, i + 1, r);
}

int main()
{
    int a[ARR_SIZE] = {2, 4, 3, 7, 8, 9, 1, 0, 5, 6};
    int l = 0, r = ARR_SIZE - 1;
    printf("qsort start\n");
    dump_arr(a, l, r);
    qsort(a, l, r);
    printf("qsort end\n");
    dump_arr(a, l, r);
    return 0;
}
