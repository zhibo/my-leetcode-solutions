#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>

#define ARR_SIZE 10

static void init_limits()
{
    struct rlimit rlim;

    system("sysctl -n -e -q -w fs.suid_dumpable=2");
    system("sysctl -n -e -q -w kernel.core_pattern=/tmp/core-%e-%p-%u-%g-%s");

    rlim.rlim_cur = RLIM_INFINITY;
    rlim.rlim_max = rlim.rlim_cur;

    setrlimit(RLIMIT_CORE, &rlim);
}

void dump_arr(int *, int, int);
inline void dump_arr(int *a, int l, int r)
{
    int i = l;
    for ( ; i <= r; ++i){
        printf("%d ", a[i]);
    }
    printf("\n");
}

// FIXME: segfault
void count_sort(int *a, int l, int r, int max)
{
    int i;
    int *b = (int *)calloc(1, (r - l + 1)*sizeof(int));
    if ( NULL == b){
        goto exit;
    }
    for (i = l; i <= r; ++i ){
        b[i] = a[i];
    }

    dump_arr(b, l, r);

    int *c = (int *)calloc(1, sizeof(int) * (max + 1));
    if ( NULL == c ){
        goto exit;
    }

    dump_arr(c, 0, ARR_SIZE);

    for ( i = l; i <= r; ++i ){
        c[b[i]] += 1;
    }

    dump_arr(c, 0, ARR_SIZE);

    for ( i = 1; i <= max; ++i ){
        c[i] += c[i - 1];
    }

    dump_arr(c, 0, ARR_SIZE);

    for ( i = max - 1; i >= 0; --i ){
        printf("%d=> c[b[i]]: %d, b[i]: %d\n", i, c[b[i]], b[i]);
        a[c[b[i]] - 1] =  b[i];
        c[b[i]] -= 1;
    }

exit:
    if ( b ){
        free(b);
    }
    if ( c ){
        free(c);
    }
}

int main()
{
    init_limits();
    int a[ARR_SIZE] = {2, 4, 3, 7, 8, 9, 1, 0, 5, 6};
    dump_arr(a, 0, ARR_SIZE - 1);
    count_sort(a, 0, ARR_SIZE - 1, ARR_SIZE);
    dump_arr(a, 0, ARR_SIZE - 1);
    return 0;
}
