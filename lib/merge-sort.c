#include <stdio.h>
#include <stdlib.h>

#define ARR_SIZE 10

void dump_arr(int *, int, int);
inline void dump_arr(int *a, int l, int r)
{
    int i = l;
    for ( ; i <= r; ++i){
        printf("%d ", a[i]);
    }
    printf("\n");
}

void merge(int *a, int l, int m, int r)
{
    int aa[ARR_SIZE] = {0};

    int i, idx_l1 = l, idx_l2= m + 1;

    for ( i = l; i <= r; ++i ){
        aa[i] = a[i];
    }

    for ( i = l; i <= r; ++i ){
        if ( idx_l1 <= m && idx_l2 <= r){
            if ( aa[idx_l1] <= aa[idx_l2] ){
                a[i] = aa[idx_l1++];
            } else {
                a[i] = aa[idx_l2++];
            }
        } else if ( idx_l1 > m ){
            a[i] = aa[idx_l2++];
        } else {
            a[i] = aa[idx_l1++];
        }
    }
}

void merge_sort(int *a, int l, int r)
{
    if ( l < r ){
        int m = l + ((r - l) >> 1);
        merge_sort(a, l, m);
        merge_sort(a, m + 1, r);
        merge(a, l, m, r);
    }
}

int main()
{
    int a[ARR_SIZE] = {2, 4, 3, 7, 8, 9, 1, 0, 5, 6};
    dump_arr(a, 0, ARR_SIZE - 1);
    merge_sort(a, 0, ARR_SIZE - 1);
    dump_arr(a, 0, ARR_SIZE - 1);
    return 0;
}
