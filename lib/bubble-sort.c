#include <stdio.h>
#define ARR_SIZE 10

#define SWAP(x, y)  \
    do{\
        x ^= y;\
        y ^= x;\
        x ^= y;\
    }while(0);

void bubble_sort(int *a, int l, int r)
{
    int i, j, flag = 1;
    for (i = l; i <= r; ++i ){
        if ( flag == 0 ){
            return;
        }
        flag = 0;
        for ( j = 1; j <= r - i; ++j ){
            if ( a[j - 1] > a[j] ){
                SWAP(a[j - 1], a[j]);
                flag = 1;
            }
        }
    }
}

int main()
{
    int a[ARR_SIZE] = {2, 4, 3, 7, 8, 9, 1, 0, 5, 6};
    dump_arr(a, 0, ARR_SIZE - 1);
    bubble_sort(a, 0, ARR_SIZE - 1);
    dump_arr(a, 0, ARR_SIZE - 1);
    return 0;
}


