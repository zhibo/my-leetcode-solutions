#include <stdio.h>
#define ARR_SIZE 10

void insert_sort(int *a, int l, int r)
{
    int i, j, key;
    for ( i = l + 1; i <= r; ++i ){
        key = a[i];
        for ( j = i - 1; j >= 0; --j ){
            if ( a[j] > key ){
                a[j+1] = a[j];
            } else {
                break;
            }
        }
        a[j + 1] = key;
    }
}

int main()
{
    int a[ARR_SIZE] = {2, 4, 3, 7, 8, 9, 1, 0, 5, 6};
    dump_arr(a, 0, ARR_SIZE - 1);
    insert_sort(a, 0, ARR_SIZE - 1);
    dump_arr(a, 0, ARR_SIZE - 1);
    return 0;
}
