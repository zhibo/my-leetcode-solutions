#include <stdio.h>
#define ARR_SIZE 10

// careful
#define SWAP(x, y)  \
    do{\
        int _a = (x); \
        int _b = (y); \
        (_a) ^= (_b);\
        (_b) ^= (_a);\
        (_a) ^= (_b);\
        x = _a; \
        y = _b; \
    }while(0);

inline void swap( int *a,  int *b)
{
    if ( a == b ){  // when a and b are the same
        return;
    }
    *a ^= *b;
    *b ^= *a;
    *a ^= *b;
}

void dump_arr( int *a, int l, int r)
{
    int i = l;
    for ( ; i <= r; ++i){
        printf("%d ", a[i]);
    }
    printf("\n");
}

void selection_sort( int *a, int l, int r)
{
    int i, j, min = l;
    for ( i = l; i <= r - 1; ++i ){
        min = i;
        for ( j = i + 1; j <= r; j++ ){
            if ( a[j] < a[min] ){
                min = j;
            }
        }
        printf("swap a[%d]:%d and a[%d]:%d\n", i, a[i], min, a[min]);
        //SWAP(a[i], a[min]);
        swap(&a[i], &a[min]);
        printf("resu a[%d]:%d and a[%d]:%d\n", i, a[i], min, a[min]);
    }
}

int main()
{
    int a[ARR_SIZE] = {2, 4, 3, 7, 8, 9, 1, 0, 5, 6};
    dump_arr(a, 0, ARR_SIZE - 1);
    selection_sort(a, 0, ARR_SIZE - 1);
    dump_arr(a, 0, ARR_SIZE - 1);
    return 0;
}
