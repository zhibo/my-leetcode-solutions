#include <stdio.h>

#define ARR_SIZE 10
#define LEFT(x) (1 + (x) * 2)
#define RIGHT(x) (2 + (x) * 2)
#define SWAP(x, y)  \
    do{\
        (x) ^= (y);\
        (y) ^= (x);\
        (x) ^= (y);\
    }while(0);

void dump_arr(int *a, int l, int r)
{
    int i = l;
    for ( ; i <= r; ++i){
        printf("%d ", a[i]);
    }
    printf("\n");
}

void max_heapify(int *a, int size, int i)
{
    int l = LEFT(i), r = RIGHT(i);
    int largest = i;

    if ( l < size && a[largest] < a[l] ){
        largest = l;
    }
    if ( r < size && a[largest] < a[r] ){
        largest = r;
    }

    if ( largest != i ){
        SWAP(a[largest], a[i]);
        max_heapify(a, size, largest);
    }
}

void build_max_heap(int *a, int size)
{
    int m = size >> 1;
    int j = m;

    for ( ; j >= 0; --j){
        max_heapify(a, size, j);
    }
}

void heap_sort(int *a, int size)
{
    build_max_heap(a, size);
    int i = size - 1;
    for ( ; i > 0; --i ){
        SWAP(a[0], a[i]);
        max_heapify(a, i, 0);
    }
}

int main()
{
    int a[ARR_SIZE] = {2, 4, 3, 7, 8, 9, 1, 0, 5, 6};
    int size = ARR_SIZE;
    printf("heap start\n");
    dump_arr(a, 0, size - 1);
    heap_sort(a, size);
    printf("heap end\n");
    dump_arr(a, 0, size - 1);
    return 0;
}
