class Solution:
    # @param A, a list of integers
    # @param target, an integer to be inserted
    # @return integer
    def searchInsert(self, A, target):
        l = 0; r = len(A) - 1
        while l < r:
            m = ( l + r ) / 2
            if A[m] < target:
                l = m + 1
            else:
                r = m
                
        return (A[l] < target) and l + 1 or l