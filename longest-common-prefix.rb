# @param {String[]} strs
# @return {String}
def longest_common_prefix(strs)
    return "" if not strs or strs.length() == 0
    str, res = strs[0], ""
    for i in 0..str.length() - 1 do
        strs.each{|x| 
          return res if i > x.length() - 1
          return res if x[i] != str[i]
        }
        res += str[i]
    end
    return res
end


result = longest_common_prefix(["aaa", "aa", "aabc"])
puts result
