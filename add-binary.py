class Solution:
    def binStrToInt(self, string):
        i = 0
        result = 0
        reverseStr = string[::-1]
        for c in reverseStr:
            if c == '1':
                result += 2 ** i
            i += 1
        return result
        
    def intToBinStr(self, integer):
        if integer == 0:
            return "0"

        result = ""
        while integer:
            if integer % 2 == 1:
                result = result + "1"
            else:
                result = result + "0"
            integer = integer / 2
        result = result[::-1]
        return result
        
    def addBinary(self, a, b):
        if not a and not b:
            return ""
        if not a:
            return b
        if not b:
            return a
        
        aInt = self.binStrToInt(a)
        bInt = self.binStrToInt(b)
        
        resultInt = aInt + bInt
        
        return self.intToBinStr(resultInt)

a = "0"
b = "0"

solution = Solution()
print solution.addBinary(a, b)

