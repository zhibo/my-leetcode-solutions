# @param {Integer[]} nums
# @return {Integer}
def max_sub_array(nums)
    hereMax, resMax = nums[0], nums[0]
    nums.slice(1, nums.length()).each{|x| hereMax = [x, hereMax + x].max; resMax = [hereMax, resMax].max}
    return resMax
end
