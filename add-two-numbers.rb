# Definition for singly-linked list.
# class ListNode
#     attr_accessor :val, :next
#     def initialize(val)
#         @val = val
#         @next = nil
#     end
# end

# @param {ListNode} l1
# @param {ListNode} l2
# @return {ListNode}
def list_to_int(l)
    ret, i = 0, 0
    while l != nil do
        ret += l.val * (10 ** i)
        l = l.next
        i += 1
    end
    return ret
end

def int_to_list(i)
    h = ListNode.new(0)
    if i == 0 then
        return h
    end
    
    p = h
    while i != 0 do
        l = ListNode.new(i % 10)
        p.next = l
        p = p.next
        i /= 10
    end
    return h.next
end

# cheating again...
def add_two_numbers(l1, l2)
    l1_int = list_to_int(l1)
    l2_int = list_to_int(l2)
    res_int = l1_int + l2_int
    return int_to_list(res_int)
end
