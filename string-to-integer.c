#include <stdio.h>

#define INT_MIN -2147483648
#define INT_MAX 2147483647

int myAtoi(char* str) {
    long result;
    /* cheating ... */
    if ( 1 != sscanf(str, "%ld", &result) ){
        return 0;
    }
    if ( result > INT_MAX ){
        result = INT_MAX;
    } else if ( result < INT_MIN ){
        result = INT_MIN;
    }
    
    return (int)result;
}

int main()
{
    char *str = "123232311111";
    printf("int: %d\n", myAtoi(str));
    return 0;
}
