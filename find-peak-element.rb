# @param {Integer[]} nums
# @return {Integer}
def find_peak_element(nums)
    len = nums.length() - 1
    l, r = 0, len
    while l < r do
        m = (l + r) / 2
        pre = m == 0 ? nums[m] - 1 : nums[m - 1]
        nxt = m == len ? nums[m] + 1 : nums[m + 1]
        return m if pre < nums[m] and nxt < nums[m]
        if pre < nums[m] and nums[m] < nxt then 
            l = m + 1   # avoid busy loop
        else 
            r = m 
        end
    end
    return l
end
