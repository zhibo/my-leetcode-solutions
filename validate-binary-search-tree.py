# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

import sys

class Solution:
    # @param root, a tree node
    # @return a boolean
    def valid(self, root, low, high):
        if not root:
            return True
        return root.val > low and root.val < high \
                and self.valid(root.left, low, root.val) \
                and self.valid(root.right, root.val, high)
                
    def isValidBST(self, root):
        return self.valid(root, -1 * sys.maxint, sys.maxint)
