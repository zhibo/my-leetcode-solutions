class Solution:
    # @return a float
    def findKthElement(self, A, B, k):
        
        lenA = len(A)
        lenB = len(B)
        
        if lenA > lenB:
            return self.findKthElement(B, A, k)
        if lenA == 0:
            return B[k-1]
        if k == 1:
            return A[0] < B[0] and A[0] or B[0]
        
        pA = k/2 < lenA and k/2 or lenA
        pB = k - pA
        
        if A[pA - 1] > B[pB - 1]:
            return self.findKthElement(A, B[pB:], k - pB)
        elif A[pA - 1] < B[pB - 1]:
            return self.findKthElement(A[pA:], B, k- pA)
        else:
            return A[pA - 1]
            
    def findMedianSortedArrays(self, A, B):
        length = len(A) + len(B)
        if length % 2 == 1:
            return self.findKthElement(A, B, length / 2 + 1)
        else:
            return (self.findKthElement(A, B, length / 2) + self.findKthElement(A, B, length / 2 + 1)) / 2.0