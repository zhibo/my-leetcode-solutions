class Solution:
    # @param num, a list of integers
    # @return an integer
    def majorityElement(self, num):
        count = 0
        for number in num:
            if count == 0:
                result = number
            if number == result:
                count += 1
            else:
                count -= 1
        
        return result