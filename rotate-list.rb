# Definition for singly-linked list.
#class ListNode
#    attr_accessor :val, :next
#    def initialize(val)
#        @val = val
#        @next = nil
#    end
#end
#
#def dump_list(head)
#  p = head
#  while p do
#    print p.val, " "
#    p = p.next
#  end
#  print "\n"
#end
#
#def build_list(a)
#  d = ListNode.new(0)
#  p = d
#  a.each{|x| n = ListNode.new(x); p.next = n; p = n}
#  return d.next
#end

# @param {ListNode} head
# @param {Integer} k
# @return {ListNode}
def rotate_right(head, k)
    return head if not head
    
    d = ListNode.new(0)
    d.next = head
    p = q = d.next
    
    i = len =  0 
    while p do
      p = p.next
      len += 1
    end

    p = d.next
    k %= len
    while i < k do
      if not q.next 
        q = d.next
      else 
        q = q.next
      end

        i += 1
    end
    
    while q.next do
        p = p.next
        q= q.next
    end
    
    q.next = d.next
    d.next = p.next
    p.next = nil
    
    return d.next
end

#head = build_list([1, 2])
#result = rotate_right(head, 2000000)
#dump_list(result)
