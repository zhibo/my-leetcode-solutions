# Definition for singly-linked list.
# class ListNode
#     attr_accessor :val, :next
#     def initialize(val)
#         @val = val
#         @next = nil
#     end
# end

# @param {ListNode} head
# @param {Integer} val
# @return {ListNode}
def remove_elements(head, val)
    d = ListNode.new(0)
    d.next = head
    p, q = d, d.next
    while p and q do
        if q.val == val then
            p.next = q.next
            q.next = nil
            q = p.next
            next
        end
        p = p.next
        q= q.next
    end
    return d.next
end

head = ListNode.new(1)
puts remove_elements(head, 2).val
