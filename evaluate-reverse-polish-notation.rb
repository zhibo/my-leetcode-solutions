# @param {String[]} tokens
# @return {Integer}
def eval_rpn(tokens)
    stack = Array.new
    tokens.each{
        |x|
        case x
            when "+"
                b = stack.pop(); a = stack.pop(); stack.push(a + b)
            when "-"
                b = stack.pop(); a = stack.pop(); stack.push(a - b)
            when "*"    
                b = stack.pop(); a = stack.pop(); stack.push(a * b)
            when "/"
                b = stack.pop(); a = stack.pop(); f = a * b < 0 ? -1 : 1; stack.push(f * (a.abs / b.abs))
            else 
                stack.push(x.to_i)
        end
    }
    return stack.pop()
end
