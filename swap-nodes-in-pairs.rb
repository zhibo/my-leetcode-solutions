# Definition for singly-linked list.
# class ListNode
#     attr_accessor :val, :next
#     def initialize(val)
#         @val = val
#         @next = nil
#     end
# end

# @param {ListNode} head
# @return {ListNode}
def swap_pairs(head)
    if head == nil then
        return head
    end
    
    d = ListNode.new(0)
    d.next = head
    k = d
    i = k.next
    j = i.next
    while j != nil do
        k.next = j
        i.next = j.next
        j.next = i
        
        k = i
        i = k.next
        if i == nil then
            break
        end
        j = i.next
    end
    return d.next
end
