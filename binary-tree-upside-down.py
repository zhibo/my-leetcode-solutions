# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param root, a tree node
    # @return root of the upside down tree
    def dfsBottomUp(self, p, parent):
        if not p:
            return parent
        node = self.dfsBottomUp(p.left, p)
        if not parent:
            p.left = parent
        else:
            p.left = parent.right
        p.right = parent
        return node
        
    def upsideDownBinaryTree(self, root):
        return self.dfsBottomUp(root, None)
