class Solution:
    # @return a tuple, (index1, index2)
    def twoSum(self, num, target):
        num1 = num2 = 0
        idx1 = idx2 = 0
        index = []
        
        for number in num:
            index.append(number)
            
        index.sort()
        i = 0
        j = len(index) - 1
        while i < j:
            if index[i] + index[j] == target:
                num1 = index[i]
                num2 = index[j]
                break
            elif index[i] + index[j] < target:
                i += 1
            else:
                j -= 1
        
        idx1 = num.index(num1)
        idx2 = num.index(num2)
        
        if num1 == num2:
            num.remove(num1)
            idx2 = num.index(num2) + 1
        
        if idx1 > idx2:
            return (idx2 + 1, idx1 + 1)
        else:
            return (idx1 + 1, idx2 + 1)

"""
# Wrong: index and count is not based on hash
class Solution:
    # @return a tuple, (index1, index2)
    def twoSum(self, num, target):
        idx1 = idx2 = 0
        num1 = num2 = 0
        for number in num:
            num1 = number
            num2 = target - number
            
            if num1 == num2:
                if num.count(num1) == 2:
                    idx1 = num.index(num1)
                    num.remove(num1)
                    idx2 = num.index(num2) + 1
                    break
                else:
                    continue

            if num2 in num:
                idx1 = num.index(num1)
                idx2 = num.index(num2)
        
        return (idx1 + 1, idx2 + 1)
"""

num = [0,2,5,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40]

target = 39

solution = Solution()
print solution.twoSum(num, target)
