# Definition for singly-linked list.
#class ListNode
#    attr_accessor :val, :next
#    def initialize(val)
#        @val = val
#        @next = nil
#    end
#end

# @param {ListNode} head
# @param {Integer} m
# @param {Integer} n
# @return {ListNode}

#def dump_list(head)
#  p = head
#  while p do
#    print p.val, " "
#    p = p.next
#  end
#  print "\n"
#end

def reverse_linked_list(head)
    k, p, q = nil, head, head.next
    while p != nil do
        p.next = k
        k = p
        p = q
        q = q.next if q
    end
    return k
end

def reverse_between(head, m, n)
    d = ListNode.new(-1)
    d.next = head
    p = l = r = b = e = d.next
    while p.next do
      p = p.next
    end

    # dump_list(d)

    p = d
    i = 0
    while p != nil do
        b = p if i == m
        e = p if i == n
        l = p if i == m - 1
        r = p if i == n + 1
        p = p.next; i += 1
    end
    r = nil if n + 1 == i
    e.next = nil

    # dump_list(b)
    b = reverse_linked_list(b)
    # dump_list(b)
    l.next = b

    # dump_list(b)
    p = b
    while p.next do
        p = p.next
    end
    p.next = r
    
    return d.next
end


#def build_list(a)
#  d = ListNode.new(0)
#  p = d
#  a.each{|x| n = ListNode.new(x); p.next = n; p = n}
#  return d.next
#end
#
#list = build_list([0,1,2,3,4])
#dump_list(list)
#
#res = reverse_between(list, 2, 2)
#dump_list(res)
