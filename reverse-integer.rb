# @param {Integer} x
# @return {Integer}

$int_min = -2147483648
$int_max = 2147483647

def reverse(x)
  ret = 0
  f = x >= 0 ? 1 : -1
  x *= f
  while x != 0 do
    ret = ret * 10 + x % 10
    x = x / 10
  end

  if f == 1 then
    ret = (ret > $int_max) ? 0 : ret
  else
    ret = (ret * f < $int_min) ? 0 : ret
  end

  return ret * f
end

print reverse(-1200000000002333232323232323)
