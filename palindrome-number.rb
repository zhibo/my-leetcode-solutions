# @param {Integer} x
# @return {Boolean}
def is_palindrome(x)
    if x < 0 then
        return false
    end
    
    t = x
    y = 0
    while x != 0 do
        y = y * 10 + x % 10
        x /= 10
    end
    
    return t == y
end
