class Solution:
    # @param s, a string
    # @return an integer
    def lengthOfLongestSubstringTwoDistinct(self, s):
        count = 0
        charMap = []
        for i in range(0, 256):
            charMap.append(-1)

        sLen = len(s)
        i = j = maxLen = 0

        while j < sLen:
            if count > 2:
                i = charMap[ord(s[j - 1])]
                charMap[ord(s[i - 1])] = -1
                count = 2

            if charMap[ord(s[j])] == -1:
                count += 1

            if j == 0 or s[j - 1] != s[j] or charMap[ord(s[j])] == -1:
                charMap[ord(s[j])] = j

            if count <= 2:
                maxLen = max(j - i + 1, maxLen)
                j += 1

        return maxLen

class Solution2:
    # @param s, a string
    # @return an integer
    def lengthOfLongestSubstringTwoDistinct(self, s):
        i = 0  # start of window
        j = -1 # just before next window
        maxLen = 0; sLen = len(s)
        for k in range(1, sLen):
            if s[k] == s[k - 1]:
                continue
            if j >= 0 and s[k] != s[j]:
                maxLen = max(k - i, maxLen)
                i = j + 1
            j = k - 1
        return max(sLen - i, maxLen)

s = Solution2()
print s.lengthOfLongestSubstringTwoDistinct("abaabaabababcdefghijklmnaopqrstuvwxyz12345")
print s.lengthOfLongestSubstringTwoDistinct("aaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbb")
print s.lengthOfLongestSubstringTwoDistinct("abcabcabc")
print s.lengthOfLongestSubstringTwoDistinct("bacc")
