#### Wrong Answer, but works on my computer ###

class MinStack:
    # @param x, an integer
    # @return an integer
    s = []
    minS = []
    
    def push(self, x):
        self.s.append(x)
        if not self.minS or x <= self.minS[-1]:
          self.minS.append(x)

    # @return nothing
    def pop(self):
        if not self.s:
            return
        x = self.s.pop()
        if x == self.minS[-1]:
            self.minS.pop()
                
    # @return an integer
    def top(self):
        return self.s[-1]

    # @return an integer
    def getMin(self):
        return self.minS[-1]

ms = MinStack()

ms.push(-1)
ms.push(-1)
ms.pop()
print ms.top()
print ms.getMin()
