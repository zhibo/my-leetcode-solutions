# @param {Integer[]} digits
# @return {Integer[]}
def plus_one(digits)
    i = digits.length - 1
    while i >= 0 do
        if digits[i] < 9 then
            digits[i] += 1
            return digits
        else
            digits[i] = 0
        end
        i -= 1
    end
    digits.insert(-1, 0)
    digits[0] = 1
    return digits
end
