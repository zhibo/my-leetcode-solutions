#include <string>
#include <iostream>

using namespace std;

class Solution {
private:
    void swapChar(char *a, char *b)
    {
        if ( !*a || !*b ){
            return;
        }
        *a ^= *b;
        *b ^= *a;
        *a ^= *b;
    }
    
    void reverseStr(string &str, int i, int j)
    {
        while ( j - i > 0 ) {
            this->swapChar(&(str[i++]), &(str[j--]));
        }
    }
    
    void trimStr(string &s)
    {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    }

    void reduceStr(string &s)
    {
        int len = s.length();
        int i = 0, j = 0;
        while ( j < len && i < len ){
            while ( s[i] != ' '){
                ++i;
                if ( i == len - 1 ){
                    break;
                }
            }

            while ( s[j] == ' '){
                ++j;
                if ( j == len - 1 ){
                    break;
                }
            }

            if ( j - i > 1 ){
                ++i;
                while ( s[j] != ' ' && j < len ){
                    s[i++] = s[j];
                    s[j++] = ' ';
                }
            }
            ++i; ++j;
        }
    }


public:
    void reverseWords(string &s) {
        int len = s.length();
        if ( len == 0 ){
            return;
        }
        
        this->reverseStr(s, 0, len - 1);

        int i = 0, j = 0;
        while ( i < len && i < len ){
            while ( s[i] == ' ' ){
                ++i; ++j;
                if ( i == len - 1 ){
                    break;
                }
            }
            
            while ( s[j] != ' ' ){
                ++j;
                if ( j == len ){
                    break;
                }
            }
            
            if ( j - i > 1 ){
                this->reverseStr(s, i, j - 1);
            }
            i = ++j;
        }
        this->reduceStr(s);
        this->trimStr(s);
    }
};

int main()
{
    Solution s;
    string str = "o jvi?pofznq ' jc?g,danm?dnb,mbzlny edgoaxr rqnlfkwilvsi ztien fgbz!nbk vmrjizqoe.cfm!cvre zzffearumnxze.f. cw!o.getu?fzpw,vfqpzeb bdxk.xa.clfmns b!n?t.dvlvj tgktrz ebykqha?sybph'pcrns ?gu,gxdc!yyl,tlqjn'jjuuvp i!qhdi' ?unmspyk!xaycl?exn'bin, t,xxdiucv tjgpon,xwdmlppcqw py?ew nyvjbhushe!oansz,enqs crpzea'o w!zu.txfuog.t'kxvhvxq? ktbduc.ta.tfvtyeuonvi.ak kr trtbifswyn.so hpkrykropdibhcn?'' ,zxzt.wrxz.dec!vllz veaw'jdsfaw,fau!ammcnos , qwetdkthrnqqjagcd'csje dafnasfntavecrnp nrk tdixikqzkqnudzaq'?r 'hjixzusd!kb edceuiks.br g. .d,ohdxequ.qsmfrqborqbb mvsjakvqqmfmcezlfje tqkcldmmwwhfckxiva ypj,yqtaxnzein.fprh tpfsdudapg?p h!umupiojfacz!. srqk e'sv!isrgebjk.n,ymaochaqh .s?zfvzabcthlheyee?lo?ljg uhwtvy.uemmm?j .vrlivecfvxqokn?, kyvquvconkk 'qtngpv?.yoxrfvelcd?'q zbdj!y!!xsvbvyt vipwd',? lqaad!?z!'wmn.nu uzhgc,ccdv!si.xgzvbga lbvf? cdjtnjbnsrtvjtba py,xllvqtdhe?vkeb!qg ?sc.bnjtwbhuvjwaz, eqy,ydprosyxrqk'!jh'lo";//abc def     asdfa  ";//abc def ";
    cout << "before" << endl << str <<  endl;
    s.reverseWords(str);
    cout << "after" << endl << str << endl;
}
