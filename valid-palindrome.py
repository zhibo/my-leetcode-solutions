"""
class Solution:
    # @param s, a string
    # @return a boolean
    def isPalindrome(self, s):
        if len(s) <= 1:
            return True
            
        l = 0; r = len(s) - 1
        while True:
            while l < r and not s[l].isalnum():
                l += 1
            while r > l and not s[r].isalnum():
                r -= 1
            if l < r and s[l].lower() == s[r].lower():
                if r - l == 1:
                    return True
                else:
                    l += 1; r -= 1
                continue
            elif l == r:
                return True
            else:
                return False
"""

class Solution:
    # @param s, a string
    # @return a boolean
    def isPalindrome(self, s):
        l = 0; r = len(s) - 1
        while l < r:
            while l < r and not s[l].isalnum():
                l += 1
            while r > l and not s[r].isalnum():
                r -= 1
            if not s[l].lower() == s[r].lower():
                return False
            l += 1; r -= 1
            
        return True
