class Solution:
    # @return an integer
    def lengthOfLongestSubstring(self, s):
        charMap = []
        for i in range(0, 256):
            charMap.append(-1)

        sLen = len(s)
        i = j = maxLen = 0

        while j < sLen:
            if charMap[ord(s[j])] >= i:
                i = charMap[ord(s[j])] + 1
            charMap[ord(s[j])] =  j
            maxLen = max(j - i + 1, maxLen)
            j += 1
        return maxLen

s = Solution()
print s.lengthOfLongestSubstring("abcdefghijklmnaopqrstuvwxyz12345")
print s.lengthOfLongestSubstring("1234563798")
