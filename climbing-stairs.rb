# @param {Integer} n
# @return {Integer}
def f(n, n_1, n_2)
    return 0 if n == 0
    return 1 if n == 1
    return 2 if n == 2
    return n_1 + n_2 if n == 3
    return f(n - 1, n_2, n_1 + n_2)
end

def climb_stairs(n)
    return f(n, 1, 2)
end
