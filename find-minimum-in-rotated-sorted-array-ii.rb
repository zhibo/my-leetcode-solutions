# @param {Integer[]} nums
# @return {Integer}
def find_min(nums)
    l, r = 0, nums.length() - 1
    while l < r and nums[l] >= nums[r]
        m = (l + r) / 2
        if nums[m] > nums[r]
            l = m + 1
        elsif nums[m] < nums[l]
            r = m
        else
            l += 1
        end
    end
    return nums[l]
end
