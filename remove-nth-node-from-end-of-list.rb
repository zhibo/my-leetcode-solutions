# Definition for singly-linked list.
# class ListNode
#     attr_accessor :val, :next
#     def initialize(val)
#         @val = val
#         @next = nil
#     end
# end

# @param {ListNode} head
# @param {Integer} n
# @return {ListNode}
def remove_nth_from_end(head, n)
    d = ListNode.new(0)
    d.next = head
    p = q = d
    for i in 0..n do
        q = q.next
    end
    
    while q do
        p = p.next
        q = q.next
    end
    
    q = p.next.next
    p.next = q
    
    return d.next
end
