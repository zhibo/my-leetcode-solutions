# @param {Integer[]} nums
# @return {Integer}
def single_number(nums)
    res = 0
    nums.each{|x| res ^= x}
    return res
end
