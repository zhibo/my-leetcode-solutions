# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param num, a list of integers
    # @return a tree node
    def buildLeft(self, node, num, left, right):
        if left > right:
            return
        mid = (left + right) / 2
        leftNode = TreeNode(num[mid])
        node.left = leftNode
        self.buildLeft(leftNode, num, left, mid - 1)
        self.buildRight(leftNode, num, mid + 1, right)
    
    def buildRight(self, node, num, left, right):
        if left > right:
            return
        mid = (left + right) / 2
        rightNode = TreeNode(num[mid])
        node.right = rightNode
        self.buildLeft(rightNode, num, left, mid - 1)
        self.buildRight(rightNode, num, mid + 1, right)
        
    def sortedArrayToBST(self, num):
        left = 0; right = len(num) - 1
        if left > right:
            return
        mid = (left + right) / 2
        tree = TreeNode(num[mid])
        self.buildLeft(tree, num, left, mid - 1)
        self.buildRight(tree, num, mid + 1, right)
        
        return tree
        
        
class Solution2:
    # @param num, a list of integers
    # @return a tree node
        
    def buildBST(self, num, left, right):
        if left > right:
            return None
        mid = (right + left) / 2
        node = TreeNode(num[mid])
        node.left = self.buildBST(num, left, mid - 1)
        node.right = self.buildBST(num, mid + 1, right)
        return node
        
    def sortedArrayToBST(self, num):
        return self.buildBST(num, 0, len(num) - 1)
