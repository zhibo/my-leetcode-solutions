class Solution:
    # @param haystack, a string
    # @param needle, a string
    # @return an integer
    def strStr(self, haystack, needle):
        hLen = len(haystack)
        nLen = len(needle)
        
        if nLen > hLen:
            return -1
        
        if nLen == hLen:
            if haystack == needle:
                return 0
            else:
                return -1
                
        if hLen < 3 or nLen < 3:
            for i in range(0, hLen + 1):
                for j in range(0, nLen + 1):
                    if j == nLen:
                        return i
                    if i + nLen > hLen:
                        return -1
                    if needle[j] != haystack[i + j]:
                        break
        
        # KMP #

        # 1. table building
        T = [0] * nLen
        T[0] = -1; T[1] = 0
        pos = 2; cnd = 0
        
        while pos < nLen:
            if needle[cnd] == needle[pos - 1]:
                cnd += 1; T[pos] = cnd; pos += 1
            elif cnd > 0:
                cnd = T[cnd]
            else:
                T[pos] = 0; pos += 1

        # 2. search
        h = n = 0 
        while h + n < hLen:
            if haystack[h + n] == needle[n]:
                if n == nLen - 1:
                    return h
                n += 1
            else:
                if T[n] > -1:
                    h += n - T[n]; n = T[n]
                else:
                    h += n - T[n]; n = 0
                
        return -1

haystack = "aaaabbbbccccdefsg"; needle = "bbbccc"
s = Solution()
print s.strStr(haystack, needle)
