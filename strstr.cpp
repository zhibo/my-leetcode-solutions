#include <string>
#include <iostream>

using namespace std;

class Solution {
private:
    void swapChar(char *a, char *b)
    {
        if ( !*a || !*b ){
            return;
        }
        *a ^= *b;
        *b ^= *a;
        *a ^= *b;
    }
    
    void reverseStr(string &str, int i, int j)
    {
        while ( j - i > 0 ) {
            this->swapChar(&(str[i++]), &(str[j--]));
        }
    }

public:
    void reverseWords(string &s) {
        int len = s.length();
        if ( len <= 1 ){
            return;
        }
        
        this->reverseStr(s, 0, len - 1);

        
        int i = 0, j = 0;
        while ( i < len ){
            while ( s[i] == ' ' ){
                ++i; ++j;
                if ( i == len - 1 ){
                    return;
                }
            }
            
            while ( s[j] != ' ' ){
                ++j;
                if ( j == len ){
                    break;
                }
            }
            
            if ( j - i > 1 ){
                this->reverseStr(s, i , j - 1);
            }
            i = ++j;
        }
    }
};

int main()
{
    Solution s;
    string str(" ");

    s.reverseWords(str);

    cout << str << endl;
}
